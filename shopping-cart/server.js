let express = require('express');
let app = express();

/// comment1123
app.get('/', function (req, res) {
    res.json({ 
        shoppingCart: [{
            product: "Alpina Watch", 
            number: 1,
            price: 500
        }, {
            product: "Nomos Watch",
            number: 2,
            price: 100
        }]
    });
    res.end();
});

app.listen(3002, function () {
  console.log("app listening on port 3002!");
});

